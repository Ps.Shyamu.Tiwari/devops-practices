

FROM openjdk:8-jdk-alpine3.9

ARG IMAGETAG

ENV ENTRY_ARG=$IMAGETAG

MAINTAINER "Shyamu Tiwari <shyamu.tiwari@clusus.com>"

RUN addgroup -S shyamu-clusus && adduser -S shyamu-clusus -G shyamu-clusus

RUN chown -R shyamu-clusus:shyamu-clusus /home

COPY target/assignment-$IMAGETAG.jar entrypoint.sh /home/

WORKDIR /home

USER shyamu-clusus

EXPOSE 8090

ENTRYPOINT ["./entrypoint.sh"]
